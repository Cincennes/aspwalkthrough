﻿using System;
using System.Collections.Generic;

namespace aspwebcore.mcc;

public partial class Track
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int Artist { get; set; }

    public int Cd { get; set; }
}
