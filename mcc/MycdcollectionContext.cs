﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace aspwebcore.mcc;

public partial class MycdcollectionContext : DbContext
{
    public MycdcollectionContext()
    {
    }

    public MycdcollectionContext(DbContextOptions<MycdcollectionContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Artist> Artists { get; set; }

    public virtual DbSet<Cd> Cds { get; set; }

    public virtual DbSet<Review> Reviews { get; set; }

    public virtual DbSet<Track> Tracks { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseMySQL("server=127.0.0.1;uid=vince;pwd=wWw6rtbrRU0i_R(o;database=mycdcollection");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Artist>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("artist");

            entity.Property(e => e.Id)
                .HasColumnType("int(11)")
                .HasColumnName("id");
            entity.Property(e => e.Name)
                .HasMaxLength(150)
                .HasColumnName("name");
        });

        modelBuilder.Entity<Cd>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("cd");

            entity.HasIndex(e => e.Review, "review");

            entity.Property(e => e.Id)
                .HasColumnType("int(11)")
                .HasColumnName("id");
            entity.Property(e => e.Acquired)
                .HasDefaultValueSql("'NULL'")
                .HasColumnType("int(11)")
                .HasColumnName("acquired");
            entity.Property(e => e.AlbumName)
                .HasMaxLength(100)
                .HasColumnName("album_name");
            entity.Property(e => e.Artist)
                .HasMaxLength(150)
                .HasColumnName("artist");
            entity.Property(e => e.CdCount)
                .HasDefaultValueSql("'NULL'")
                .HasColumnType("int(11)")
                .HasColumnName("cd_count");
            entity.Property(e => e.Localcover)
                .HasDefaultValueSql("'NULL'")
                .HasColumnType("text")
                .HasColumnName("localcover");
            entity.Property(e => e.Netcover)
                .HasDefaultValueSql("'NULL'")
                .HasColumnType("text")
                .HasColumnName("netcover");
            entity.Property(e => e.Review)
                .HasMaxLength(150)
                .HasDefaultValueSql("'NULL'")
                .HasColumnName("review");
        });

        modelBuilder.Entity<Review>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("review");

            entity.HasIndex(e => e.CdId, "cd_id");

            entity.Property(e => e.Id)
                .HasColumnType("int(11)")
                .HasColumnName("id");
            entity.Property(e => e.CdId)
                .HasComment("foreign key for cd")
                .HasColumnType("int(11)")
                .HasColumnName("cd_id");
            entity.Property(e => e.Review1)
                .HasColumnType("text")
                .HasColumnName("review");
        });

        modelBuilder.Entity<Track>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("tracks");

            entity.HasIndex(e => e.Artist, "artist");

            entity.HasIndex(e => e.Cd, "cd");

            entity.Property(e => e.Id)
                .HasColumnType("int(11)")
                .HasColumnName("id");
            entity.Property(e => e.Artist)
                .HasColumnType("int(11)")
                .HasColumnName("artist");
            entity.Property(e => e.Cd)
                .HasColumnType("int(11)")
                .HasColumnName("cd");
            entity.Property(e => e.Name)
                .HasColumnType("text")
                .HasColumnName("name");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
