﻿using System;
using System.Collections.Generic;

namespace aspwebcore.mcc;

public partial class Review
{
    public int Id { get; set; }

    public string Review1 { get; set; } = null!;

    /// <summary>
    /// foreign key for cd
    /// </summary>
    public int CdId { get; set; }
}
