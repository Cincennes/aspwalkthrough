﻿using System;
using System.Collections.Generic;

namespace aspwebcore.mcc;

public partial class Cd
{
    public int Id { get; set; }

    public string AlbumName { get; set; } = null!;

    public int? CdCount { get; set; }

    public string Artist { get; set; } = null!;

    public string? Review { get; set; }

    public int? Acquired { get; set; }

    public string? Localcover { get; set; }

    public string? Netcover { get; set; }
}
