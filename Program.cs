// I think it will work this way
using aspwebcore.mcc;

#region Configure host and services
var builder = WebApplication.CreateBuilder(args);
// Raser pages addon
builder.Services.AddRazorPages();
builder.Services.AddHealthChecks();
var app = builder.Build();
#endregion

#region is in dev mode?
if (!app.Environment.IsDevelopment())
{
  app.UseHsts();
}
app.UseHttpsRedirection();
#endregion// This will use secure connect if the app is not in development

#region Endpoints
app.MapRazorPages();
app.MapGet("/", () => "");
app.MapGet("/ping", () => "Pong");
// my get route for the envrionment
app.MapGet("/env", () => $"Environment is {app.Environment.EnvironmentName} \n {app.Environment.WebRootPath} \n {app.Environment.WebRootFileProvider} \n {app.Environment.ContentRootPath} \n {app.Environment.ContentRootFileProvider}");
#endregion

app.UseDefaultFiles();
app.UseStaticFiles();
app.Run();
